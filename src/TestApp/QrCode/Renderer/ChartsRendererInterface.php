<?php

namespace MyApp\TestApp\QrCode\Renderer;

/**
 * Interface ChartsRendererInterface
 */
interface ChartsRendererInterface
{

    /**
     * @param $text
     * @param $width
     * @param $height
     * @return mixed
     */
    public function generate($text, $width, $height);
}
