<?php

namespace MyApp\Tests\TestApp;

use MyApp\TestApp\QrCode;
use MyApp\TestApp\QrCode\Renderer\GoogleChartsRenderer;

/**
 * Class QrCodeTest
 */
class QrCodeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test TestApp initialization
     *
     * @return void
     */
    public function testConstructorWithoutParameters()
    {
        $renderer = new GoogleChartsRenderer();

        $qrCode = $this->getMockBuilder('MyApp\TestApp\QrCode')
            ->setMethods(['setRenderer', 'getDafaultRenderer'])
            ->disableOriginalConstructor()
            ->getMock();

        $qrCode->expects($this->once())
            ->method('setRenderer');

        $qrCode->expects($this->once())
            ->method('getDafaultRenderer')
            ->willReturn($renderer);

        $qrCode->__construct();
    }

    /**
     *
     */
    public function testConstructorWithParameters()
    {
        $renderer = new GoogleChartsRenderer();

        $qrCode = $this->getMockBuilder('MyApp\TestApp\QrCode')
            ->setMethods(['setRenderer', 'getDafaultRenderer'])
            ->disableOriginalConstructor()
            ->getMock();

        $qrCode->expects($this->once())
            ->method('setRenderer')
            ->with($this->equalTo($renderer));

        $qrCode->expects($this->never())
            ->method('getDafaultRenderer');

        $qrCode->__construct($renderer);
    }

    /**
     *
     */
    public function testGenerate()
    {
        $renderer = $this->getMockBuilder('MyApp\TestApp\QrCode\Renderer\GoogleChartsRenderer')
            ->disableOriginalConstructor()
            ->setMethods(['generate'])
            ->getMock();

        $renderer->expects($this->once())
            ->method('generate')
            ->with('testText', 100, 150)
            ->willReturn('testData');

        $qrCode = $this->getMockBuilder('MyApp\TestApp\QrCode')
            ->setMethods(['getRenderer'])
            ->disableOriginalConstructor()
            ->getMock();

        $qrCode->expects($this->once())
            ->method('getRenderer')
            ->willReturn($renderer);

        $qrCode->generate('testText', 100, 150);
    }

    public function providerTestGenerateException()
    {
        return array(
            array('', 100, 100),
            array('Test', 0, 100),
            array('Test', 100, 0),
            array('Test', -100, 0),
            array('Test', 100, -100),
            array('Test', 'asd', 100),
            array('Test', 100, 'asd'),
        );
    }

    /**
     * @dataProvider providerTestGenerateException
     * @expectedException InvalidArgumentException
     */
    public function testGenerateException($text, $width, $height)
    {
        $renderer = $this->getMock('MyApp\TestApp\QrCode\Renderer\GoogleChartsRenderer');

        $qrCode = $this->getMockBuilder('MyApp\TestApp\QrCode')
            ->setMethods(['getRenderer'])
            ->disableOriginalConstructor()
            ->getMock();

        $qrCode->expects($this->never())
            ->method('getRenderer')
            ->willReturn($renderer);

        $qrCode->generate($text, $width, $height);
    }
}
