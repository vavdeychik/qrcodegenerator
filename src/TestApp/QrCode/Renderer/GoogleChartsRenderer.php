<?php

namespace MyApp\TestApp\QrCode\Renderer;

/**
 * Class GoogleChartsRenderer
 */
class GoogleChartsRenderer implements ChartsRendererInterface
{
    /**
     * Returns QR Code Image Data or throw exception on error
     *
     * @param $data
     * @param $width
     * @param $height
     * @return string
     */

    public function generate($data, $width, $height)
    {

        if (empty($data)) {
            throw new \InvalidArgumentException('Data of the QrCode must be specified');
        }

        if (empty($width) || !is_int($width) || $width <= 0) {
            throw new \InvalidArgumentException('Width of the QrCode must be specified and be positive number');
        }

        if (empty($height) || !is_int($height) || $height <= 0) {
            throw new \InvalidArgumentException('Height of the QrCode must be specified and be positive number');
        }

        $params = array(
            'cht' => 'qr',
            'choe' => 'UTF-8',
            'chl' => $data,
            'chs' => $width . 'x' . $height
        );

        $url = 'https://chart.googleapis.com/chart?' . http_build_query($params);


        $qrCode = file_get_contents($url);

        if ($qrCode == false){
            throw new Exception('Could not retrive QrCode from Google server');
        }

        return $qrCode;
    }
}
