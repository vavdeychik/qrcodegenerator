<?php

namespace MyApp\TestApp;

use MyApp\TestApp\QrCode\Renderer\ChartsRendererInterface;
use MyApp\TestApp\QrCode\Renderer\GoogleChartsRenderer;

class QrCode
{
    protected $renderer;

    public function __construct(ChartsRendererInterface $renderer = null)
    {
        if (!empty($renderer)) {
            $this->setRenderer($renderer);
        } else {
            $this->setRenderer($this->getDafaultRenderer());
        }

    }

    /**
     * @param ChartsRendererInterface $renderer
     */
    public function setRenderer(ChartsRendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return mixed
     */
    protected function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param null $text
     * @param null $width
     * @param null $height
     * @return mixed
     * @throws \Exception
     */
    public function generate($text = null, $width = null, $height = null)
    {
        if (empty($text)) {
            throw new \InvalidArgumentException('Text of the QrCode must be specified');
        }

        if (empty($width) || !is_int($width) || $width <= 0) {
            throw new \InvalidArgumentException('Width of the QrCode must be specified and be positive number');
        }

        if (empty($height) || !is_int($height) || $height <= 0) {
            throw new \InvalidArgumentException('Height of the QrCode must be specified and be positive number');
        }

        return $this->getRenderer()->generate($text, $width, $height);
    }

    /**
     * @return GoogleChartsRenderer
     */
    protected function getDafaultRenderer()
    {
        return new GoogleChartsRenderer();
    }


}

