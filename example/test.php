<?php

require_once(__DIR__ . '/../vendor/autoload.php');

use MyApp\TestApp\QrCode;
use MyApp\TestApp\QrCode\Renderer\GoogleChartsRenderer;

/**
 * The original  API was
 *  $qrCode = new QrCode('TrekkSoft', 50, 50); // text, width, height
 * $qrCode->setRenderer(new GoogleChartsRenderer());
 * $qrCodeData = $qrCode->generate();
 *
 * But in this case if we need to generate a lot of QrCodes we'll need to
 * create a new object on each image
 *
 * I prefere to Ingect the renderer in constructor class
 * and pass data to genearate method.
 *
 * Of course you can change renderer calling $qrCode->setRenderer();
 * My API is:
 *
 * $qrCode = new QrCode();   // Use default renderer
 * $qrCode->generate('TestText', 150, 150);
 *
 * OR
 *
 * $qrCode = new QrCode(new GoogleChartsRenderer());
 * $qrCode->generate('TestText', 150, 150);
 *
 * OR
 *
 * $qrCode = new QrCode();
 * $qrCode->setRenderer(new GoogleChartsRenderer());  // in case we need to change renderer on the fly
 * $qrCode->generate('TestText', 150, 150);
 *
 */

$qrCode = new QrCode();

try {
    $qrCodeData = $qrCode->generate('TestText', 150, 150);
    $fileName = 'qrCode.png';
    file_put_contents($fileName, $qrCodeData);
    echo "Done! See generated QrCode in the file: $fileName" . PHP_EOL;
} catch (\Exception $e) {
    echo 'Error: ' . $e->getMessage() . PHP_EOL;
}
