QRCodeGenerator
===============

Generates a QR code by default using Google's API. 

  The original API was

    $qrCode = new QrCode('TrekkSoft', 50, 50); // text, width, height
    $qrCode->setRenderer(new GoogleChartsRenderer());
    $qrCodeData = $qrCode->generate();
 
  But in this case if we need to generate a lot of QrCodes we'll need to create a new object for each image
 
  I prefere to Ingect the renderer in constructor class and pass data to genearate method.
 
  Of course we can change renderer calling $qrCode->setRenderer();


API
---

    $qrCode = new QrCode();   // Uses default renderer
    $qrCode->generate('TestText', 150, 150);
 
OR
 
    $qrCode = new QrCode(new GoogleChartsRenderer());
    $qrCode->generate('TestText', 150, 150);
 
OR
 
    $qrCode = new QrCode();
    $qrCode->setRenderer(new GoogleChartsRenderer());  // in case we need to change renderer on the fly
    $qrCode->generate('TestText', 150, 150);
 
 
EXAMPLE
-------

    use MyApp\TestApp\QrCode;
    use MyApp\TestApp\QrCode\Renderer\GoogleChartsRenderer;
    $qrCode = new QrCode();

    try {
      $qrCodeData = $qrCode->generate('TestText', 150, 150);
      $fileName = 'qrCode.png';
      file_put_contents($fileName, $qrCodeData);
      echo "Done! See generated QrCode in the file: $fileName" . PHP_EOL;
    } catch (\Exception $e) {
      echo 'Error: ' . $e->getMessage() . PHP_EOL;
    }
